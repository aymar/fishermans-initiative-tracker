from random import randint
from xml.etree import ElementTree
from xml.dom import minidom
import os
import sys

def get_command(message):
    if sys.version_info >= (3, 0):
        return input(message)
    else:
        return raw_input(message)

def cls():
    os.system('cls' if os.name == 'nt' else 'clear')
    
def prettify(xmlroot):
    rough_string = ElementTree.tostring(xmlroot, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="    ")
    
class Settings:
    def __init__(self):
        try:
            e = ElementTree.parse('settings.xml').getroot()
            self.vanilla = e.get('vanilla') == 'True'      
        except:
            self.vanilla = False
            
    def export(self):
        xmlroot = ElementTree.Element('settings', {'vanilla':('True' if self.vanilla else 'False')})
        xmlfile = open('settings.xml', 'w')
        xmlfile.write(prettify(xmlroot))

class Character:
    def __init__(self):
        self.name = 'Default'
        self.modifier = 0
        self.advantage = False
        self.disadvantage = False
        self.last_initiative = 0
        
    def __init__(self, name, modifier, advantage, disadvantage):
        self.name = name
        self.modifier = modifier
        self.advantage = advantage
        self.disadvantage = disadvantage   
        self.last_initiative = 0
        
    def _is_valid_operand(self, other):
        return hasattr(other, "last_initiative")
        
    def __eq__(self, other):
        if not self._is_valid_operand(other):
            return NotImplemented            
        return self.last_initiative == other.last_initiative and self.modifier == other.modifier
        
    def __lt__(self, other):
        if not self._is_valid_operand(other):
            return NotImplemented            
        return self.last_initiative > other.last_initiative or (self.last_initiative == other.last_initiative and self.modifier > other.modifier)
        
    def roll_initiative(self):
        roll = randint(1, 20)
        if self.advantage and not self.disadvantage:
            roll = max(roll, randint(1, 20))
        if self.disadvantage and not self.advantage:  
            roll = min(roll, randint(1, 20))
        self.last_initiative = roll + self.modifier
    
    def print_initiative(self):
        print('\t-> ' + self.name + ', initiative: ' + str(self.last_initiative) + ' (mod: ' + str(self.modifier) + ')')
        
    def print_self(self):
        print('\t-> ' + self.name + ', initiative mod: ' + str(self.modifier) + (' (advantage)' if self.advantage and not self.disadvantage else '') + (' (disadvantage)' if self.disadvantage and not self.advantage else ''))

def read_characters(xmlfile):
    players = []
    e = ElementTree.parse(xmlfile).getroot()
    for atype in e.findall('character'):
        name = atype.get('name')
        modifier = int(atype.get('modifier'))
        advantage = True if (atype.get('advantage') == 'yes') else False
        disadvantage = True if (atype.get('disadvantage') == 'yes') else False
        players.append(Character(name, modifier, advantage, disadvantage))
    return players
        
players = read_characters('players.xml')
settings = Settings()

def header():
    cls()
    print ('Welcome to Fisherman\'s Initiative Tracker!')
    print ('You can reach me at aymar.fisherman@gmail.com')
    print ('I hope you enjoy it. Cheers!')
    print ('')
    print ('')
    print ('Players:')
    for player in players:
        player.print_self()
    print ('')
    print ('')
    print ('Settings:')
    print ('\tVanilla mode is ' + ('on' if settings.vanilla else 'off') + ', initiatives are rolled once per ' + ('battle' if settings.vanilla else 'round'))
    print ('')
    print ('')

header()

def run_battle_against(monsters):
    global settings
    global players
    
    cls()
    print ('Battle started')
    print ('')
    print ('')
    
    all_involved = []    
    print ('Players:')
    for player in players:
        all_involved.append(player)
        player.print_self()        
    print ('')
    print ('Enemies:')
    for monster in monsters:
        all_involved.append(monster)
        monster.print_self()
    print ('')
    print ('')
    
    char_id = -1
    round_counter = 1
    markers = []
    
    for char in all_involved:
        char.roll_initiative()
    all_involved.sort()        
    
    print ('Turn order (round ' + str(round_counter) + '):')
    if len(markers) > 0:
        for i in range(0, len(markers)):
            print('\tMarker ' + str(i) + ': ' + markers[i])
        print ('')
    for char in all_involved:
        char.print_initiative()
    print ('')
    print ('')
    
    while True:
        command = get_command('Type a command ("help"):')
        print ('')
        
        if command == 'next':
            if char_id >= len(all_involved) - 1:
                char_id = -1
                round_counter += 1
                if not settings.vanilla:
                    for char in all_involved:
                        char.roll_initiative()
                    all_involved.sort()  
                print ('New round starting!!!')
                print ('Turn order (round ' + str(round_counter) + '):')
                if len(markers) > 0:
                    for i in range(0, len(markers)):
                        print('\tMarker ' + str(i) + ': ' + markers[i])
                    print ('')
                for char in all_involved:
                    char.print_initiative()
                print ('')
                    
            print ('Top of the round is:')  
            print ('')  
            char_id += 1
            all_involved[char_id].print_initiative()
            print ('')
            print ('')
            
        elif command == 'next round':
            char_id = -1
            round_counter += 1            
            if not settings.vanilla:
                for char in all_involved:
                    char.roll_initiative()
                all_involved.sort()            
            print ('Turn order (round ' + str(round_counter) + '):')
            if len(markers) > 0:
                for i in range(0, len(markers)):
                    print('\tMarker ' + str(i) + ': ' + markers[i])
                print ('')
            for char in all_involved:
                char.print_initiative()
            print ('')
            print ('')
            
        elif command == 'summary':            
            print ('Turn order (round ' + str(round_counter) + '):')
            if len(markers) > 0:
                for i in range(0, len(markers)):
                    print('\tMarker ' + str(i) + ': ' + markers[i])
                print ('')
            for char in all_involved:
                char.print_initiative()
            print ('')
            print ('')
            
        elif command.startswith('add marker'):
            commandList = command.split(' ', 2)
            if len(commandList) != 3:
                print ('"add marker" command expects 1 argument, ' + str(len(commandList) - 1) + ' supplied.')
                print ('')
                print ('')
                continue            
            new_marker = commandList[2] + ' (on round ' + str(round_counter) + ')'
            markers.append(new_marker)
            
            print('Successfully added marker ' + commandList[2])            
            print ('')
            print ('')
            
        elif command.startswith('remove marker'):
            commandList = command.split(' ')
            if len(commandList) != 3:
                print ('"remove marker" command expects 1 argument, ' + str(len(commandList) - 1) + ' supplied.')
                print ('')
                print ('')
                continue
            
            idToDel = -1
            try:
                idToDel = int(commandList[2])
            except:
                print ('"remove marker" command expects an index as argument (integer number).')
                print ('')
                print ('')
                continue
                
            print ('Successfully removed marker \'' + markers[idToDel] + '\'')
            del markers[idToDel] 
            print ('')
            print ('')
            
        elif command.startswith('add characters'):
            commandList = command.split(' ', 2)
            if len(commandList) != 3:
                print ('"add characters" command expects 1 argument, ' + str(len(commandList) - 1) + ' supplied.')
                print ('')
                print ('')
                continue            
            try:
                more_chars = []
                more_chars = read_characters(commandList[2])                
                for char in more_chars:
                    all_involved.append(char)
                    char.roll_initiative()
                    if char < all_involved[char_id]:
                        char_id += 1
                all_involved.sort()
                
                print('Successfully added characters from ' + commandList[2])
            except:
                print('Could not locate file ' + commandList[1])                
            print ('')
            print ('')
            
        elif command.startswith('kill'):
            commandList = command.split(' ', 1)
            if len(commandList) != 2:
                print ('"kill" command expects 1 argument, ' + str(len(commandList) - 1) + ' supplied.')
                continue
            idToDel = -1
            for i in range(0, len(all_involved)):
                if all_involved[i].name.lower() == commandList[1].lower():
                    idToDel = i
                    break
            if idToDel != -1:
                print ('Successfully killed character ' + all_involved[idToDel].name)
                if idToDel <= char_id:
                    char_id -= 1
                del all_involved[idToDel]           
            else:
                print ('Could not find a character named ' + commandList[1] + ' to kill.')
            print ('')
            print ('')
            
        elif command == "help":
            print ('Available commands:')
            print ('"next"')
            print ('\tAdvances to the next character.')
            print ('"next roud"')
            print ('\t(Alternative to "next") Advances to the next round.')
            print ('"summary"')
            print ('\tPrints round summary (turn order, markers, etc).')
            print ('"add marker [marker_name]"')
            print ('\tAdds a marker to the round. Ex: add marker Brann casted Haste (duration: 10 rounds)')
            print ('"remove marker [marker_index]"')
            print ('\tRemoves a marker by index. Ex: remove marker 2')
            print ('"add characters [morecharacters.xml]"')
            print ('\tAdds characters to the battle. Ex: add characters orcs.xml')
            print ('"kill [character_name]"')
            print ('\tExcludes a character from the battle. Ex: kill Dire Wolf')            
            print ('"end"')
            print ('\tEnds the battle and exits Battle Mode.')
            print ('')
            print ('')
            
        elif command == 'end':
            header()
            return
            
        else:
            print ('Invalid command.')
            print ('')
            print ('')

while True:        
    command = get_command('Type a command ("help"):')
    print ('')
    
    if command.startswith('battle'):
        commandList = command.split(' ', 1)
        if len(commandList) != 2:
            print ('"battle" command expects 1 argument, ' + str(len(commandList) - 1) + ' supplied.')
            print ('')
            print ('')
            continue     

        try:
            monsters = []
            monsters = read_characters(commandList[1])
            run_battle_against(monsters)
        except:
            print('Could not locate file ' + commandList[1])
            print ('')
            print ('')
            continue
            
    elif command == "toggle vanilla":
        settings.vanilla = not settings.vanilla
        settings.export()
        print ('Vanilla system is now ' + ('on' if settings.vanilla else 'off') + ', initiatives will be rolled once per ' + ('combat' if settings.vanilla else 'round'))
        print ('')
        print ('')
        
    elif command == "help":
        print ('Available commands:')
        print ('"battle [encounterfile.xml]"')
        print ('\tInitiates a battle against the enemies in the xml file. Ex: "battle wolves.xml"')
        print ('"toggle vanilla"')
        print ('\tToggles vanilla system (initiatives rolled once per combat) on/off.')        
        print ('"exit"')
        print ('\tExits the application.')
        print ('')
        print ('')
            
    elif command == 'exit':
        break
    
    else:
        print ('Invalid command.')
        print ('')
        print ('')