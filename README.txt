Author: Aymar Pescador Jr

How to use:
- Fill in the players.xml with all your PCs and NPCs helping the party.
- Create encounters xml (one for each battle)
- Run the fisherman_initiative_tracker.py and follow the instructions.

Example:
- After setting up your players.xml run the script.
- (Optional) Toggle vanilla mode if you don't want initiatives to the re-rolled each round.
- Type "battle wolves.xml" to enter battle mode.

Battle Mode:
    - Type "next" to show the next character to take a turn.
    - Type "next round" to advance to the next round (alternative to "next").
    - Type "summary" to show the current turn order.
    - Type "add marker [marker name and details]" to add a marker to the current round.
        Ex: add marker Brann casted Haste on Boris (duration: 10)
    - Type "remove marker [marker number]" to remove a marker.
    - Type "add characters [xmlfilename]" to add more friends or foes to the battle.
        Ex: add characters orcs.xml
    - Type "kill [character name]" to remove a character from the battle.
        Ex: kill Dire Wolf
    - Type "end" to end the battle and exit Battle Mode.

Regardind "kill" command:
    When you "kill" a character, it is removed from the battle, so if the character is still rolling death saves, don't kill him yet.

Regarding markers:
    These are used to add annotations to the current round. 
    For instance: "add_marker Brann casted Haste on Thor (duration: 10)" will add this annotation to the current round, the actual marker will be: "Brann casted Haste on Thor (duration: 10) (on round 3)" assuming the current round is the third round.
    Then, in each round, you'll be able to compare the round Haste was casted against the current round number. This serves as a reminder for when to end duration spells and effects.

License: MIT License (included)